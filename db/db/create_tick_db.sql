GO
/****** Object:  Table [dbo].[Tick]    Script Date: 7/19/2016 9:52:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tick](
	[TickId] [int] IDENTITY(1,1) NOT NULL,
	[TickTime] [datetime] NOT NULL,
	[AppVersion] [nvarchar](50) NOT NULL,
	[Hostname] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TickId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
