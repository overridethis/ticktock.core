﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TickTock.Core.Models;

namespace TickTock.Core
{
  public interface ITickService
  {
    Task<IEnumerable<Tick>> Query();

    Task Tick(Tick tick);
  }
}
