﻿using Microsoft.EntityFrameworkCore;
using TickTock.Core.Models;

namespace TickTock.Core
{
  public class TickDbContext : DbContext
  {
    public TickDbContext(DbContextOptions<TickDbContext> options) : base(options)
    {
    }

    public DbSet<Tick> Ticks { get; set; }
  }
}
