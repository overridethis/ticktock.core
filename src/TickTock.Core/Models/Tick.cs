﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace TickTock.Core.Models
{
  [Table("Tick", Schema="dbo")]
  public class Tick
  {
    [Key]
    public int TickId { get; set; }
    public DateTime TickTime { get; set; }
    public string AppVersion { get; set; }
    public string Hostname { get; set; }
  }
}
