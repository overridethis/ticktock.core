﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TickTock.Core.Models;

namespace TickTock.Core.Impl
{
  public class TickService : ITickService
  {
    private readonly TickDbContext _dbContext;

    public TickService(TickDbContext dbContext)
    {
      _dbContext = dbContext;
    }

    public async Task<IEnumerable<Tick>> Query()
    {
      return await _dbContext.Ticks
       .OrderByDescending(t => t.TickTime)
       .Take(10)
       .ToListAsync();
    }

    public async Task Tick(Tick tick)
    {
      _dbContext.Ticks.Add(tick);
      await _dbContext.SaveChangesAsync();
    }
  }
}